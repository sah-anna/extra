package com.example.java;

import java.util.Scanner;


// Implement a Java Console program
// that can help you to print
// the reverse value of a specified string.
public class ReverseString {

    public static void run() {
        System.out.println("Print the reverse value of a string");

        String str;

        if (Main.getUserInput()) {
            System.out.println("User input chosen");
            str = getString();
        } else {
            System.out.println("Hardcode value chosen");
            str = "А роза упала на лапу Азора";
        }

        reverse(str);
    }

    // gets user-defined string
    private static String getString() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String result = sc.nextLine();
//        sc.close();
        return result;
    }

    // reverses value of a specified string and prints reversed string
    private static void reverse(String str) {
        char[] array = str.toCharArray();
        int last = array.length - 1;
        for (int i = last; i >= 0; --i) {
            System.out.print(array[i]);
        }
        System.out.println();
    }

}
