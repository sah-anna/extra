package com.example.java;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        MinMax.run();
        System.out.println("--------");
        ReverseString.run();
        System.out.println("--------");
    }

    // gets user-defined choice of input type
    // returns true if manual input is chosen,
    //         false for predefined hardcode values
    public static boolean getUserInput() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input manually? (y/n): ");
        boolean result = sc.nextLine().equalsIgnoreCase("y");
//        sc.close();
        return result;
    }

}
