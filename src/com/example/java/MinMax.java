package com.example.java;

import java.util.InputMismatchException;
import java.util.Scanner;


// Implement a Java Console program
// that can help you to determine and print
// Max and Min values from a fixed array of numbers.
public class MinMax {

    public static void run() {
        System.out.println("Determine and print Max and Min values from an array of numbers");

        int[] array;

        if (Main.getUserInput()) {
            System.out.println("User input chosen");
            try {
                array = getIntArray();
            } catch (InputMismatchException e) {
                System.out.println("Not a number!");
                return;
            } catch (Exception e) {
                System.out.println("Something gone wrong!");
                return;
            }
        } else {
            System.out.println("Hardcode values chosen");
            array = new int[] {15, -394, 0, 81, 12, -394};
        }

        find(array);
    }

    // gets user-defined array of integer numbers
    private static int[] getIntArray() {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter array size: ");
        int size = sc.nextInt();

        int[] result = new int[size];

        System.out.print("Enter array elements: ");
        for (int i = 0; i < size; ++i) {
            result[i] = sc.nextInt();
        }

//        sc.close();
        return result;
    }

    // determines and prints Max and Min values from a fixed array of numbers
    private static void find(int[] array) {

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int element: array) {
            min = Math.min(element, min);
            max = Math.max(element, max);
        }

        System.out.println("Min value: " + min);
        System.out.println("Max value: " + max);
    }

}
